# Tools for Deformetrica 

## Getting Started

This project contains independent functions written with Python to be used to prepare or to process data for the software Deformetrica (http://www.deformetrica.org).

### Prerequisites

Some functions need Meshlab or Paraview (see function documentation).<br/>
For changing Paraview pvpython path, please modify src/var.py

Installation with anaconda :<br/>
```
conda create -n toolsfordeformetrica
conda activate toolsfordeformetrica
conda install pip
pip install nibabel
pip install Pillow
pip install torch
```

### Installing

To run a function: <br/>
```
$ cd /path/to/function
$ conda activate toolsfordeformetrica
$ python function_name.py
```

### List of functions

* src/preprocessing/atlas_file_edition.py: This function creates all files used to estimate an atlas from a set of surfaces.
* src/preprocessing/atlas_file_edition_olympe.py: This function creates all files used to estimate an atlas from a set of surfaces for the Olympe server.
* src/preprocessing/pairwise_file_edition_olympe.py: This function creates all files used to compute pairwise from a folder of surfaces for the Olympe server.
* src/preprocessing/ply_clean_osx_directory.py: This function cleans surfaces (PLY format) to be used with deformetrica.
* src/preprocessing/ply2vtk_directory.py: This function converts a dataset of PLY files into VTK format using pvpython from Paraview.
* src/preprocessing/vtk_clean_osx_directory.py: This program cleans surfaces (VTK format) to be used with deformetrica. 
* src/preprocessing/vtk2ply_diretory.py: This program convert a dataset of VTK files into PLY format using pvpython from Paraview.
* src/server_tools/server_matching_combination.py: This function generate pairwise matching to be launched with deformetrica. 
* src/server_tools/server_matching_combination_add.py: This function generate pairwise matching to be launched with deformetrica when adding surfaces.
* src/server_tools/server_matching_combination_olympe.py: This function generate pairwise matching to be launched with deformetrica and the supercomputer Olympe.
* src/server_tools/server_matching_combination_add_olympe.py: This function generate pairwise matching to be launched with deformetrica when adding surfaces and the supercomputer Olympe.
* src/postprocessing/add_colormap.py: This function adds colormaps to the output of Deformetrica (surface atlas and matching). See example below.

![colormap_example](/images/GIFDemiSphere_to_subject_3_.gif)

* src/postprocessing/add_colormap_shooting.py: This function adds colormaps to the output of Deformetrica (surface shooting). 
* src/postprocessing/add_signed_colormap.py This function adds signed colormaps to the output of Deformetrica (surface atlas and matching). See example below.

![colormap_example](/images/signed_colormap_DeterministicAtlas__flow__tooth__subject_subj3.gif)

* src/postprocessing/add_local_maximal_momenta.py This function creates a vtk file which can be visualised as vectors (glyph) using Paraview. See example below.

![colormap_example](/images/local_momenta_vectors.png)

* src/postprocessing/color_transfer.py: This function copies the colormap from the first surface to the second surface

![transfer_example](/images/transfer.png)

* src/postprocessing/screenshot_directory.py: This program takes screenshots in Paraview of ply files for pairwise combinations.
* src/postprocessing/setup_ShootAndFlow_mean.py: This program generates data for group mean computations


## Author

* [**Jean Dumoncel**](https://gitlab.com/jeandumoncel) 

## License

Copyright [2019] [Jean Dumoncel]

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.