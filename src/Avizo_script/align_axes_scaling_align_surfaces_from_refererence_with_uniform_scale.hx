# Amira-Script-Object V3.0

#########################################################################
#
# Align surfaces
#
# author: Jean Dumoncel
#
#########################################################################


remove -all
set dir "/Volumes/lacie/tmp/clean_aligned"
set reference "/Volumes/lacie/tmp/clean_aligned/ref.ply"




foreach plyfiles [glob -directory $dir *.ply] {



	if {[string compare $plyfiles $reference] != 0} {
		[load $reference] setLabel "mesh1"
		[load $plyfiles] setLabel "mesh2"

		set apa [create HxAlignPrincipalAxes {AlignPrincipalAxes}]
		AlignPrincipalAxes Model connect mesh2
		AlignPrincipalAxes Reference connect mesh1
		AlignPrincipalAxes fire

		$apa action touch
		$apa action hit
		$apa fire

		mesh2 applyTransform

		set bb1 [mesh1 getBoundingBox]
		set bb2 [mesh2 getBoundingBox]
		set d1 [expr (sqrt(pow([lindex $bb1 0] - [lindex $bb1 3],2) + pow([lindex $bb1 1] - [lindex $bb1 4],2) + pow([lindex $bb1 2] - [lindex $bb1 5],2))) ]
		set d2 [expr (sqrt(pow([lindex $bb2 0] - [lindex $bb2 3],2) + pow([lindex $bb2 1] - [lindex $bb2 4],2) + pow([lindex $bb2 2] - [lindex $bb2 5],2))) ]
		set factor [expr $d1 / $d2]
		mesh2 setScaleFactor  $factor $factor $factor

		mesh2 applyTransform

		set as [create HxAlignSurfaces {AlignSurfaces}]
		"AlignSurfaces" surface_to_be_transformed connect mesh2
		"AlignSurfaces" reference_surface connect mesh1
		$as fire
		"AlignSurfaces" options setValue 1 0
		"AlignSurfaces" trafo setValue 1
		"AlignSurfaces" stop setValues 0.001 30
		# "AlignSurfaces" align setState "index 0 newflag -1"

		$as  align setValue 1
		$as fire

		$as  align setValue 0
		$as fire

		$as  align setValue 0
		$as fire

		$as  align setValue 0
		$as fire

		$as  align setValue 0
		$as fire

		mesh2 applyTransform
		set t [string range $plyfiles 0 end-4]
		set result [append t "_aligned.ply"]

		"mesh2" save "Stanford PLY" $result
		remove -all
	}
}
