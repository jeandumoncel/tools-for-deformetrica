# align_surfaces_from_refererence_with_uniform_scale

## Getting Started

This Avizo script allow to sign a set of surfaces from a reference. Surfaces are first aligned using the "Align Principal Axes" toll in Avizo. Then Surfaces are rotated, translated and scaled to fit the reference surface using the "Align surfaces" tool in Avizo.

Two parameters:

the folder containing the ply files:

set dir "/Volumes/lacie/tmp/clean_aligned"

the reference surface:

set reference "/Volumes/lacie/tmp/clean_aligned/ref.ply"

## Author

* [**Jean Dumoncel**](https://gitlab.com/jeandumoncel)

## License

Copyright [2019] [Jean Dumoncel]

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
