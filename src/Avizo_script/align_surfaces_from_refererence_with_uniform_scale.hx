# Amira-Script-Object V3.0

#########################################################################
#
# Align surfaces from refererence with uniform scale
#
# author: Jean Dumoncel
#
#########################################################################

# Amira Script
remove -all
set dir "/Volumes/lacie/tmp/clean_aligned"
set reference "/Volumes/lacie/tmp/clean_aligned/ref.ply"




foreach plyfiles [glob -directory $dir *.ply] {



	if {[string compare $plyfiles $reference] != 0} { 
		[load $reference] setLabel "mesh1"
		[load $plyfiles] setLabel "mesh2"

		set as [create HxAlignSurfaces {AlignSurfaces}]
		"AlignSurfaces" surface_to_be_transformed connect mesh2
		"AlignSurfaces" reference_surface connect mesh1
		$as fire
		"AlignSurfaces" options setValue 1 0
		"AlignSurfaces" trafo setValue 1
		# "AlignSurfaces" align setState "index 0 newflag -1"

		$as  align setValue 1
		$as fire

		$as  align setValue 0
		$as fire

		$as  align setValue 0
		$as fire

		$as  align setValue 0
		$as fire

		$as  align setValue 0
		$as fire

		mesh2 applyTransform
		set t [string range $plyfiles 0 end-4]
		set result [append t "_aligned.ply"]

		"mesh2" save "Stanford PLY" $result
		remove -all
	}
}
