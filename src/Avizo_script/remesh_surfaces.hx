# Amira-Script-Object V3.0

#########################################################################
#
# Remesh surfaces
#
# author: Jean Dumoncel
#
#########################################################################

# Amira Script
set dir "E:/En_cours/etudiants/Elsa/surface_email_dentine/M2"

foreach mesh [glob -directory $dir *.ply] {
	remove -all
	[load $mesh] setLabel "mesh"
	
	set rs [create HxRemeshSurface {RemeshSurface}]
	RemeshSurface data connect mesh
	{RemeshSurface} fire
	RemeshSurface desiredSize setValue 1 100000
	RemeshSurface fire
	
	$rs  remesh setValue 0
	$rs fire
	
	set t [string range $mesh 0 end-4]
	set result [append t "_remeshed.ply"]
	"mesh.remeshed" save "Stanford PLY" $result
	remove -all
	}