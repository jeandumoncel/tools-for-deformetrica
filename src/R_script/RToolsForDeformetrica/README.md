# R Tools for Deformetrica 

## Getting Started

This project contains functions written with R to process data from Deformetrica (http://www.deformetrica.org).

### Prerequisites

### Dependencies

RGL package

geometry package

### Building the package

In a terminal :

R CMD build RToolsForDeformetrica

### Installing

From your local build:

install.packages("RToolsForDeformetrica_0.1.tar.gz", repos = NULL, type = "source")

From the gitlab site: 

download.file(
  "https://gitlab.com/jeandumoncel/tools-for-deformetrica/-/raw/master/src/R_script/RToolsForDeformetrica/RToolsForDeformetrica_0.1.tar.gz", 
  "RToolsForDeformetrica_0.1.tar.gz"
)

install.packages("RToolsForDeformetrica_0.1.tar.gz", repos = NULL, type = "source")

### Examples

Example codes are given in the documentation

Principal Component Analysis with convex hulls:

![2D_PCA_with_convex_hull](/src/R_script/RToolsForDeformetrica/images/pca_2d_convex_hull.png)

Principal Component Analysis in 3D with projections:

![3D_PCA_with_convex_hull_and_projections](/src/R_script/RToolsForDeformetrica/images/pca_3d_convex_hull.png)

Morphospace:
![Morphospace](/src/R_script/RToolsForDeformetrica/images/pca_2d_convex_hull_morphospace.png)

## Author

* [**Jean Dumoncel**](https://gitlab.com/jeandumoncel) 

## License

Copyright [2020] [Jean Dumoncel]

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.