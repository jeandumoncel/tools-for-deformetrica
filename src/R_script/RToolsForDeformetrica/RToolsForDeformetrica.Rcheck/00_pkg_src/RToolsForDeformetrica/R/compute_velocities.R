compute_velocities <-
function (MOM, CP, kernelwidth) {
  NDim = 3
  CPx  = t(matrix(rep(CP[1,],dim(MOM)[2]/NDim), ncol = dim(MOM)[2]/NDim))
  CPy  = t(matrix(rep(CP[2,],dim(MOM)[2]/NDim), ncol = dim(MOM)[2]/NDim))
  CPz  = t(matrix(rep(CP[3,],dim(MOM)[2]/NDim), ncol = dim(MOM)[2]/NDim))
  K = exp(-( (CPx - t(CPx))^2 + (CPy - t(CPy))^2 + (CPz - t(CPz))^2 )/kernelwidth^2)
  V1 = K %*% t(MOM[,seq(from = 1, to = dim(MOM)[2], by = 3)])
  V2 = K %*% t(MOM[,seq(from = 2, to = dim(MOM)[2], by = 3)])
  V3 = K %*% t(MOM[,seq(from = 3, to = dim(MOM)[2], by = 3)])
  V = matrix(nrow=dim(MOM)[1],ncol=dim(MOM)[2])
  V[,seq(from = 1, to = dim(MOM)[2], by = 3)] = t(V1)
  V[,seq(from = 2, to = dim(MOM)[2], by = 3)] = t(V2)
  V[,seq(from = 3, to = dim(MOM)[2], by = 3)] = t(V3)
  return(V)
}
