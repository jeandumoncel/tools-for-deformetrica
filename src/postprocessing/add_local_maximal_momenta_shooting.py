#!/usr/bin/env python3

"""
Created on Mon May 10 10:35:02 2021

@author: Jean Dumoncel



"""

from pathlib import Path
from tkinter import filedialog
from tkinter import *
import sys
sys.path.insert(0, '..')
from deformetrica.in_out.deformable_object_reader import DeformableObjectReader
import numpy as np
from landmarkscalar import LandmarkScalar
from deformetrica.in_out.xml_parameters import XmlParameters


def ismember(a, b):
    return [np.sum(aa == b) for aa in a]


def add_local_maximal_momenta_shooting(path_name, prefix):
    number_of_points = 11
    expected = [None] * number_of_points
    expected_dimension = [None] * number_of_points
    expected_connectivity = [None] * number_of_points
    d = [None] * number_of_points
    for k in range(0, number_of_points):
        expected[k], expected_dimension[k], expected_connectivity[k] = DeformableObjectReader.read_vtk_file(
            Path(path_name, prefix + '%d__age_%1.02f.vtk' % (k, k / 10)), extract_connectivity=True)
        if k==0:
            d[k] = np.zeros(len(expected[k]))
        else:
            d[k] = d[k-1] + np.sqrt(np.sum((expected[k] - expected[k - 1]) ** 2, axis=1))

    Path.mkdir(path_name / 'colormaps', exist_ok=True)

    points = []
    dv = []

    for k in range(0, len(expected[len(expected)-1])):
        # print(len(expected[len(expected)-1]))
        ind = np.where(np.array(np.isin(expected_connectivity[len(expected_connectivity)-1][:, 0], [k])) |
                       np.array(np.isin(expected_connectivity[len(expected_connectivity)-1][:, 1], [k])) |
                       np.array(np.isin(expected_connectivity[len(expected_connectivity)-1][:, 2], [k])))
        indpts = np.unique(expected_connectivity[len(expected_connectivity)-1][ind, :])
        ind = np.where(np.array(np.isin(expected_connectivity[len(expected_connectivity)-1][:, 0], [indpts])) |
                       np.array(np.isin(expected_connectivity[len(expected_connectivity)-1][:, 1], [indpts])) |
                       np.array(np.isin(expected_connectivity[len(expected_connectivity)-1][:, 2], [indpts])))
        indpts = np.unique(expected_connectivity[len(expected_connectivity)-1][ind, :])
        ind = np.where(np.array(np.isin(expected_connectivity[len(expected_connectivity)-1][:, 0], [indpts])) |
                       np.array(np.isin(expected_connectivity[len(expected_connectivity)-1][:, 1], [indpts])) |
                       np.array(np.isin(expected_connectivity[len(expected_connectivity)-1][:, 2], [indpts])))
        indpts = np.unique(expected_connectivity[len(expected_connectivity)-1][ind, :])

        if len(indpts) > 0 and all(np.less_equal(d[len(d)-1][indpts], d[len(d)-1][k])):
            points.append(expected[len(expected)-1][k, :])
            dv.append(expected[len(expected)-1][k, :]-expected[0][k, :])

    surface = LandmarkScalar()
    surface.points = points
    surface.connectivity = None
    surface.normals = dv
    surface.write_points(path_name / 'colormaps', prefix + 'local_maximal_momenta.vtk')




def add_local_maximal_momenta_shooting_directory(root_directory=None):

    if root_directory is None:
        # window for choosing a directory
        root = Tk()
        root.withdraw()  # use to hide tkinter window
        root_directory = filedialog.askdirectory(initialdir="~/",  title='Please select a directory containing files')

    root_directory = Path(root_directory)

    xml_parameters = XmlParameters()
    xml_parameters.read_all_xmls(root_directory / 'model.xml', None, root_directory /
                                 'optimization_parameters.xml', root_directory / 'output')

    for obj in xml_parameters.template_specifications:
        new_string = ('Shooting__GeodesicFlow__%s__tp_' % (obj))
        add_local_maximal_momenta_shooting(root_directory / 'output', new_string)





def main():
    """
    main
    """
    add_local_maximal_momenta_shooting_directory()


if __name__ == "__main__":
    main()
