#!/usr/bin/env python3

"""
Created on Mon May 20 10:35:02 2019

@author: Jean Dumoncel

This function adds signed colormaps to the output of Deformetrica (surface atlas and matching). The input directory must
contain the xml parameter files (named 'model.xml',
'data_set.xml' and 'optimization_parameters.xml') and the 'output' folder.
The outputs (in folder "colormaps") contain a scalar field representing the cumulative distance from the initial
surface for each vertex.
Signed colors are obtained by comparing the directions of the displacements and the vertex normals.

"""

from pathlib import Path
from tkinter import filedialog
from tkinter import *
import sys
sys.path.insert(0, '..')
from deformetrica.in_out.deformable_object_reader import DeformableObjectReader
import numpy as np
from landmarkscalar import LandmarkScalar
from deformetrica.in_out.xml_parameters import XmlParameters


def normalize_v3(arr):
    # code from https://sites.google.com/site/dlampetest/python/calculating-normals-of-a-triangle-mesh-using-numpy
    ''' Normalize a numpy array of 3 component vectors shape=(n,3) '''
    lens = np.sqrt(arr[:, 0] ** 2 + arr[:, 1] ** 2 + arr[:, 2] ** 2)
    lens[lens == 0] = 1
    arr[:, 0] /= lens
    arr[:, 1] /= lens
    arr[:, 2] /= lens
    return arr


def compute_normals(vertices, faces):
    # code from https://sites.google.com/site/dlampetest/python/calculating-normals-of-a-triangle-mesh-using-numpy
    # Create a zeroed array with the same type and shape as our vertices i.e., per vertex normal
    norm = np.zeros(vertices.shape, dtype=vertices.dtype)
    # Create an indexed view into the vertex array using the array of three indices for triangles
    tris = vertices[faces]
    # Calculate the normal for all the triangles, by taking the cross product of the vectors v1-v0, and v2-v0 in each
    # triangle
    n = np.cross(tris[::, 1] - tris[::, 0], tris[::, 2] - tris[::, 0])
    # n is now an array of normals per triangle. The length of each normal is dependent the vertices,
    # we need to normalize these, so that our next step weights each normal equally.
    normalize_v3(n)
    # now we have a normalized array of normals, one per triangle, i.e., per triangle normals.
    # But instead of one per triangle (i.e., flat shading), we add to each vertex in that triangle,
    # the triangles' normal. Multiple triangles would then contribute to every vertex, so we need to normalize again
    # afterwards.
    # The cool part, we can actually add the normals through an indexed view of our (zeroed) per vertex normal array
    norm[faces[:, 0]] += n
    norm[faces[:, 1]] += n
    norm[faces[:, 2]] += n
    normalize_v3(norm)
    return norm


def add_signed_colormap(path_name, prefix, number_of_time_points):
    expected = [None] * number_of_time_points
    expected_dimension = [None] * number_of_time_points
    expected_connectivity = [None] * number_of_time_points
    expected_normals = [None] * number_of_time_points
    for k in range(0, number_of_time_points):
        expected[k], expected_dimension[k], expected_connectivity[k] = \
            DeformableObjectReader.read_vtk_file(Path(path_name, prefix + '%d.vtk' % k), extract_connectivity=True)
        expected_normals[k] = compute_normals(expected[k], expected_connectivity[k])

    Path.mkdir(path_name / 'colormaps', exist_ok=True)

    surface = LandmarkScalar()
    surface.points = expected[0]
    surface.connectivity = expected_connectivity[0]
    surface.scalars = np.zeros(len(surface.points))
    surface.write(path_name / 'colormaps', 'signed_colormap_' + prefix + '%d.vtk' % 0)

    d = 0

    sign = [None] * (number_of_time_points-1)

    for k in range(1, number_of_time_points):
        normalPoints = expected_normals[k-1]
        sign[k-1] = np.sign(np.sum(normalPoints * (expected[k] - expected[0]), axis=1))
        # sign[k - 1] = np.sign(np.einsum("ij,ij->i", expected[k], expected[0]))
    s = np.sign(np.sum(sign, axis=0))
    s[s == 0] = 1

    for k in range(1, number_of_time_points):
        surface = LandmarkScalar()
        surface.points = expected[k]
        surface.connectivity = expected_connectivity[k]
        d = d + np.sqrt(np.sum((expected[k]-expected[k-1]) ** 2, axis=1))
        surface.scalars = d * s
        if any(np.isnan(d)):
            print(np.argwhere(np.isnan(d)))
        surface.write(path_name / 'colormaps', 'signed_colormap_' + prefix + '%d.vtk' % k)


def add_signed_colormap_directory(root_directory=None):

    if root_directory is None:
        # window for choosing a directory
        root = Tk()
        root.withdraw()  # use to hide tkinter window
        root_directory = filedialog.askdirectory(initialdir="~/",  title='Please select a directory containing files')

    root_directory = Path(root_directory)

    xml_parameters = XmlParameters()
    xml_parameters.read_all_xmls(root_directory / 'model.xml', root_directory / 'data_set.xml', root_directory /
                                 'optimization_parameters.xml', root_directory / 'output')

    cpt = 0
    for obj in xml_parameters.dataset_filenames:
        new_string = ('DeterministicAtlas__flow__%s__subject_%s__tp_' % (list(obj[0].keys())[0],
                                                                         xml_parameters.subject_ids[cpt]))
        add_signed_colormap(root_directory / 'output', new_string, xml_parameters.number_of_time_points)
        cpt = cpt + 1


def main():
    """
    main
    """
    add_signed_colormap_directory()


if __name__ == "__main__":
    main()
