#!/usr/bin/env python3

"""
Created on Mon Aug 14 10:22:02 2019

@author: Jean Dumoncel

This function copies the colormap from the first surface to the second surface

"""

from pathlib import Path
from tkinter import filedialog
from tkinter import *
import numpy as np
from landmarkscalar import LandmarkScalar
from deformable_object_reader_scalar import DeformableObjectReaderScalar


def color_transfer(surface1=None, surface2=None):

    if surface1 is None:
        # window for choosing the initial surface
        root = Tk()
        root.withdraw()  # use to hide tkinter window
        surface1 = filedialog.askopenfilename(initialdir="~/", title="Select initial colored vtk surface")

    if surface2 is None:
        # window for choosing the target surface
        root = Tk()
        root.withdraw()  # use to hide tkinter window
        surface2 = filedialog.askopenfilename(initialdir="~/", title="Select target vtk surface")

    surface1 = Path(surface1)
    surface2 = Path(surface2)

    colored, colored_dimension, colored_connectivity, colored_scalars = DeformableObjectReaderScalar.read_vtk_file(
        surface1, extract_connectivity=True, scalar=True)

    target, target_dimension, target_connectivity = DeformableObjectReaderScalar.read_vtk_file(
        surface2, extract_connectivity=True, scalar=False)

    target_scalars = np.zeros((len(target)))
    for pointTarget, indice in zip(target, range(len(target))):
        d = np.zeros((len(colored)))
        for ti in range(3):
            d = d + np.square(colored[:, ti]-pointTarget[ti])
        d = np.sqrt(d)
        target_scalars[indice] = colored_scalars[int(np.where(d == np.amin(d))[0][0])]

    surface = LandmarkScalar()
    surface.points = target
    surface.connectivity = target_connectivity
    surface.scalars = target_scalars
    surface.write(surface1.parents[0], surface2.stem + '_ct.vtk')


def main():
    """
    main
    """
    color_transfer()


if __name__ == "__main__":
    main()
