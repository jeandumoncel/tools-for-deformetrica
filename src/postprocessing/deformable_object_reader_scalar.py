"""
Created on Mon Aug 14 10:27:02 2019

@author: Jean Dumoncel

overload DeformableObjectReader class for vtk file reading

"""

import sys
sys.path.insert(0, '..')
from deformetrica.in_out.array_readers_and_writers import *
from deformetrica.in_out.deformable_object_reader import DeformableObjectReader


class DeformableObjectReaderScalar(DeformableObjectReader):

    @staticmethod
    def read_vtk_file(filename, dimension=None, extract_connectivity=False, scalar=False):
        """
        Routine to read vtk files
        Probably needs new case management
        code modified from deformetrica: src/in_out/deformable_object_reader.py
        """

        with open(filename, 'r') as f:
            content = f.readlines()
        fifth_line = content[4].strip().split(' ')

        assert fifth_line[0] == 'POINTS'
        assert fifth_line[2] == 'float'

        nb_points = int(fifth_line[1])
        points = []
        line_start_connectivity = None
        connectivity_type = nb_faces = nb_vertices_in_faces = None

        if dimension is None:
            dimension = DeformableObjectReaderScalar.__detect_dimension(content)

        assert isinstance(dimension, int)
        # logger.debug('Using dimension ' + str(dimension) + ' for file ' + filename)

        # Reading the points:
        flagconnectivity = False
        for i in range(5, len(content)):
            line = content[i].strip().split(' ')
            # Saving the position of the start for the connectivity
            if line == ['']:
                continue
            elif line[0] in ['LINES', 'POLYGONS']:
                line_start_connectivity = i
                connectivity_type, nb_faces, nb_vertices_in_faces = line[0], int(line[1]), int(line[2])
                flagconnectivity = True
            elif line[0] in ['CELL_DATA']:
                line_start_scalar = i
                break
            elif not flagconnectivity:
                points_for_line = np.array(line, dtype=float).reshape(int(len(line) / 3), 3)[:, :dimension]
                for p in points_for_line:
                    points.append(p)
        points = np.array(points)
        assert len(points) == nb_points, 'Something went wrong during the vtk reading'

        # Reading the connectivity, if needed.
        if extract_connectivity:
            # Error checking
            if connectivity_type is None:
                RuntimeError('Could not determine connectivity type.')
            if nb_faces is None:
                RuntimeError('Could not determine number of faces type.')
            if nb_vertices_in_faces is None:
                RuntimeError('Could not determine number of vertices type.')

            if line_start_connectivity is None:
                raise KeyError('Could not read the connectivity for the given vtk file')

            connectivity = []

            for i in range(line_start_connectivity + 1, line_start_connectivity + 1 + nb_faces):
                line = content[i].strip().split(' ')
                number_vertices_in_line = int(line[0])

                if connectivity_type == 'POLYGONS':
                    assert number_vertices_in_line == 3, 'Invalid connectivity: deformetrica only handles triangles ' \
                                                         'for now.'
                    connectivity.append([int(elt) for elt in line[1:]])
                elif connectivity_type == 'LINES':
                    assert number_vertices_in_line >= 2, 'Should not happen.'
                    for j in range(1, number_vertices_in_line):
                        connectivity.append([int(line[j]), int(line[j + 1])])

            connectivity = np.array(connectivity)

            # Some sanity checks:
            if connectivity_type == 'POLYGONS':
                assert len(connectivity) == nb_faces, 'Found an unexpected number of faces.'
                assert len(connectivity) * 4 == nb_vertices_in_faces

            if scalar:

                scalars = []

                for i in range(line_start_scalar + 4, line_start_scalar + 4 + nb_points):
                    line = content[i].strip().split(' ')
                    scalars.append(float(line[0]))
                return points, dimension, connectivity, scalars

            return points, dimension, connectivity

        return points, dimension

    @staticmethod
    def __detect_dimension(content, nb_lines_to_check=2):
        """
        Try to determine dimension from VTK file: check last element in first nb_lines_to_check points to see if filled
         with 0.00000, if so 2D else 3D
        :param content:     content to check
        :param nb_lines_to_check:   number of lines to check
        :return:    detected dimension
        code copied from deformetrica: src/in_out/deformable_object_reader.py
        """

        assert nb_lines_to_check > 0, 'You must check at least 1 line'

        dimension = None

        for i in range(5, 5+nb_lines_to_check-1):
            line_elements = content[i].split(' ')
            if float(line_elements[2]) == 0.:
                if dimension is not None and dimension == 3:
                    raise RuntimeError('Could not automatically determine data dimension. Please manually specify '
                                       'value.')
                dimension = 2
            elif float(line_elements[2]) != 0.:
                if dimension is not None and dimension == 2:
                    raise RuntimeError('Could not automatically determine data dimension. Please manually specify '
                                       'value.')
                dimension = 3
            else:
                raise RuntimeError('Could not automatically determine data dimension. Please manually specify value.')

        return dimension
