#!/usr/bin/env python3

"""
Created on Wed Apr  9 14:02:45 2019

@author: Jean Dumoncel

This program takes screenshots in Paraview of ply files for pairwise combinations.

"""

from pathlib import Path
from tkinter import filedialog
from tkinter import *
from os import system, path, mkdir
import sys
sys.path.insert(0, '..')
import var


def screenshot_pairwise(*args):
    # Parview script
    script_path = 'screenshot_script.py'
    if len(args) < 2:
        # window for choosing a directory
        root = Tk()
        root.withdraw()  # use to hide tkinter window
        input_directory = Path(filedialog.askdirectory(initialdir="~/",  title='Please select a directory containing '
                                                                               'mesh files'))
        root = Tk()
        root.withdraw()  # use to hide tkinter window
        output_directory = filedialog.askdirectory(initialdir="~/", title='Please select the output directory')
    else:
        input_directory = args[0]
        output_directory = args[1]

    if not(path.isdir(output_directory)):
        mkdir(output_directory)

    # list directories
    dirnames = list(Path(input_directory).glob('*'))
    for dirname in dirnames:
        if path.isdir(dirname):
            bn = path.basename(dirname)
            sourceFile = path.join(dirname, 'input1.vtk')
            targetFile = path.join(dirname, 'input2.vtk')
            resultFile = path.join(dirname, 'output', 'DeterministicAtlas__flow__body__subject_subj2__tp_19.vtk')
            if path.isfile(sourceFile) and path.isfile(targetFile) and path.isfile(resultFile):
                system(var.pvpython_path + ' ' + script_path + ' ' + sourceFile + ' ' + targetFile + ' ' + output_directory
                       + ' ' + bn + ' 1')
                system(var.pvpython_path + ' ' + script_path + ' ' + targetFile + ' ' + resultFile + ' ' + output_directory
                       + ' ' + bn + ' 3')
            else:
                print("Some files are missing")


def main():
    """
    main
    """
    screenshot_pairwise()


if __name__ == "__main__":
    main()
