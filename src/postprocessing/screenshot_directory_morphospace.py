#!/usr/bin/env python3

"""
Created on Wed Apr  9 14:02:45 2019

@author: Jean Dumoncel

This program takes screenshots in Paraview of ply files for pairwise combinations.

"""

from pathlib import Path
from tkinter import filedialog
from tkinter import *
from os import system, path, mkdir
import sys
sys.path.insert(0, '..')
import var


def screenshot_pairwise(*args):
    # Parview script
    script_path = 'screenshot_script_morphospace.py'
    d_object = 'endocast'
    if len(args) < 2:
        # window for choosing a directory
        root = Tk()
        root.withdraw()  # use to hide tkinter window
        input_directory = Path(filedialog.askdirectory(initialdir="~/",  title='Please select a directory containing mesh files'))
        root = Tk()
        root.withdraw()  # use to hide tkinter window
        output_directory = filedialog.askdirectory(initialdir=input_directory, title='Please select the output directory')
    else:
        input_directory = args[0]
        output_directory = args[1]

    if not(path.isdir(output_directory)):
        mkdir(output_directory)

    # list directories
    dirnames = list(Path(input_directory).glob('*'))
    for dirname in dirnames:
        if path.isdir(dirname):
            bn = path.basename(dirname)
            resultFile = path.join(dirname, 'output', 'colormaps', 'Shooting__GeodesicFlow__'+d_object+'__tp_10__age_1.00.vtk')
            print(resultFile)
            if path.isfile(resultFile):
                #windows
                print('" "' + var.pvpython_path + '" ' + script_path + ' "' + resultFile + '" "' + ' "' + bn + '" "' + ' "' + output_directory + '" "')
                system('" "' + var.pvpython_path + '" ' + script_path + ' "' + resultFile + '" "' + ' "' + bn + '" "' + ' "' + output_directory + '" "')
                #mac
                #system(var.pvpython_path + ' ' + script_path + ' ' + resultFile + ' ' + bn + ' ' + output_directory)
            else:
                print("Some files are missing")


def main():
    """
    main
    """
    screenshot_pairwise()


if __name__ == "__main__":
    main()
