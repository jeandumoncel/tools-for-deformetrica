#!/usr/bin/env python3

"""
Created on Wed Apr  9 14:11:18 2019

@author: Jean Dumoncel
"""



from screenshot_directory_morphospace import *

morphospace_folder = '"C:/Users/Palevoprim/Documents/computations/test_5_5_0.001/stats"'
images_folder = "C:/Users/Palevoprim/Documents/computations/test_5_5_0.001/stats_images"

screenshot_pairwise(morphospace_folder, images_folder)
