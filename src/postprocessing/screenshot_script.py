#!/usr/bin/env python3

"""
Created on Wed Apr  9 14:11:18 2019

@author: Jean Dumoncel
"""

from paraview.simple import *
from os import path

paraview.simple._DisableFirstRenderCameraReset()
filename1 = sys.argv[1]
base1 = path.splitext(filename1)[0]
vtk_file1 = LegacyVTKReader(FileNames=filename1)
filename2 = sys.argv[2]
base2 = path.splitext(filename2)[0]
vtk_file2 = LegacyVTKReader(FileNames=filename2)
output_directory = sys.argv[3]
bn = sys.argv[4]
nb = int(sys.argv[5])

renderView1 = GetActiveViewOrCreate('RenderView')
vtk_file1Display1 = Show(vtk_file1, renderView1)
vtk_file1Display2 = Show(vtk_file2, renderView1)
vtk_file1Display1 .DiffuseColor = [1.0, 0.0, 0.0]
renderView1.Update()

renderView1 = GetActiveViewOrCreate('RenderView')
renderView1.ResetCamera()
renderView1.Update()
renderView1.OrientationAxesVisibility = 0
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [24.55495038464416, -7.381658742207655, 13.521540255823592]
renderView1.CameraFocalPoint = [-7.710959143785993, 2.0925415732062347, -5.558547305060439]
renderView1.CameraViewUp = [-0.49464874013653987, 0.061300990372820344, 0.8669283779301755]
renderView1.CameraParallelScale = 10.07008512438277
renderView1.Update()
ratio = renderView1.GetPropertyValue('ViewSize')[0] / renderView1.GetPropertyValue('ViewSize')[1]
width = 300
renderView1.ViewSize = [width, width/ratio]

layout = GetLayout()
SaveScreenshot(path.join(output_directory, bn + '_0' + str(nb) + '.jpg'), layout, TransparentBackground=1,
               view=renderView1, quality=100, magnification=1)

renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [-24.72200427511331, 0.9376023344320579, -14.103783495679249]
renderView1.CameraFocalPoint = [9.085650780312864, -0.7554205686460895, 4.579772476014514]
renderView1.CameraViewUp = [0.4802128505579912, -0.06446115252814541, -0.8747801883751778]
renderView1.CameraParallelScale = 10.07008512438277

ratio = renderView1.GetPropertyValue('ViewSize')[0] / renderView1.GetPropertyValue('ViewSize')[1]
renderView1.ViewSize = [width, width/ratio]

layout = GetLayout()
SaveScreenshot(path.join(output_directory, bn + '_0' + str(nb+1) + '.jpg'), layout, TransparentBackground=1,
               view=renderView1, quality=100, magnification=1)
