#!/usr/bin/env python3

"""
Created on Wed Apr  9 14:11:18 2019

@author: Jean Dumoncel
"""

from paraview.simple import *
from os import path

paraview.simple._DisableFirstRenderCameraReset()
filename1 = sys.argv[1]
base = sys.argv[2]
vtk_file1 = LegacyVTKReader(FileNames=filename1)
output_directory = sys.argv[3]


renderView1 = GetActiveViewOrCreate('RenderView')
vtk_file1Display1 = Show(vtk_file1, renderView1)
# change solid color
# vtk_file1Display1.DiffuseColor = [0.7333333333333333, 0.7333333333333333, 0.7333333333333333]
# get color transfer function/color map for 'scalars'
scalarsLUT = GetColorTransferFunction('scalars')
# Rescale transfer function
scalarsLUT.RescaleTransferFunction(0, 5)

renderView1.Update()

renderView1 = GetActiveViewOrCreate('RenderView')
renderView1.ResetCamera()
renderView1.Update()
renderView1.OrientationAxesVisibility = 0


renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [-11.414520304699607, 167.12636895345236, -9.987088291742289]
renderView1.CameraFocalPoint = [-0.18360346926849003, -0.2496629800964681, 1.844278208925199]
renderView1.CameraViewUp = [0.9671361642932896, 0.08190948487449903, 0.24070412543963016]
renderView1.CameraParallelScale = 51.129901251749324


renderView1.Update()
#ratio = renderView1.GetPropertyValue('ViewSize')[0] / renderView1.GetPropertyValue('ViewSize')[1]
#width = 300
#renderView1.ViewSize = [width, width/ratio]
renderView1.ViewSize = [780, 1468]

renderView1.Update()

#layout = GetLayout()
SaveScreenshot(path.join(output_directory, base.strip() + '.png'), renderView1)
