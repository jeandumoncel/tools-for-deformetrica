#!/usr/bin/env python3

"""
Created on Thu Apr 15 10:35:02 2021

@author: Jean Dumoncel

This function computes the momenta means for one or several groups.

"""

from tkinter import filedialog
from tkinter import *
from tkinter import simpledialog
import sys
sys.path.insert(0, '..')
from deformetrica.in_out.array_readers_and_writers import read_3D_array
from deformetrica.in_out.xml_parameters import XmlParameters
import re
import numpy as np
from pathlib import Path
from os import path
from shutil import copyfile


def write_model_xml_shooting(root_directory, xml_parameters):
    file = open(root_directory / "model.xml", "w")
    file.write("<?xml version=\"1.0\"?>\n")
    file.write("<model>\n")
    file.write("    <model-type>%s</model-type>\n" % xml_parameters.model_type)
    file.write("    <dimension>%s</dimension>\n" % xml_parameters.dimension)
    file.write("    <dtype>float32</dtype>\n")
    file.write("    <template>\n")
    file.write("        <object id=\"%s\">\n" % xml_parameters.object_id)
    file.write("            <deformable-object-type>%s</deformable-object-type>\n" %
               xml_parameters.deformable_object_type)
    file.write("	    <attachment-type>%s</attachment-type>\n" % xml_parameters.attachment_type)
    file.write("            <noise-std>%s</noise-std>\n" % xml_parameters.noise_std)
    file.write("            <kernel-type>%s</kernel-type>\n" % xml_parameters.object_kernel_type)
    file.write("            <kernel-width>%s</kernel-width>\n" % xml_parameters.object_kernel_width)
    file.write("            <filename>%s</filename>\n" % ('DeterministicAtlas__EstimatedParameters__Template_%s.vtk' % xml_parameters.object_id))
    file.write("        </object>\n")
    file.write("    </template>\n")
    file.write("    <initial-control-points>DeterministicAtlas__EstimatedParameters__ControlPoints.txt</initial-control-points>\n")
    file.write("    <initial-momenta>%s</initial-momenta>\n" % xml_parameters.initial_momenta)
    file.write("    <deformation-parameters>\n")
    file.write("        <kernel-width>%s</kernel-width>\n" % xml_parameters.deformation_kernel_width)
    file.write("        <kernel-type>%s</kernel-type>\n" % xml_parameters.kernel_type)
    file.write("        <number-of-timepoints>%s</number-of-timepoints>\n" % xml_parameters.number_of_time_points)
    file.write("    </deformation-parameters>\n")
    file.write(" </model>\n")
    file.close()


def write_launch_simulation_shooting_sh(root_directory):
    file = open(root_directory / "launch_simulation.sh", "wb")
    file.write(
        b"\ndeformetrica compute model.xml -p optimization_parameters.xml --output=output -v DEBUG > logout.txt\n")
    file.close()


def write_3D_array(array, output_dir, name):
    """
    modified from deformetrica/in_out/array_readers_and_writers.py
    Saving an array has dim (numsubjects, numcps, dimension), using deformetrica format
    """
    s = array.shape
    if len(s) == 2:
        array = np.array([array])
    save_name = path.join(output_dir, name)
    with open(save_name, "w") as f:
        for elt in array:
            for elt1 in elt:
                for elt2 in elt1:
                    f.write(str(elt2) + " ")
                f.write("\n")
            f.write("\n")


def setup_ShootAndFlow_mean(root_directory, groups, group_names):

    root_directory = Path(root_directory)
    xml_parameters = XmlParameters()
    xml_parameters.read_all_xmls(root_directory / 'model.xml', root_directory / 'data_set.xml', root_directory /
                                 'optimization_parameters.xml', root_directory / 'output')

    xml_parameters.object_id = list(xml_parameters.template_specifications.keys())[0]
    xml_parameters.deformable_object_type = xml_parameters.template_specifications[xml_parameters.object_id][
        'deformable_object_type']
    xml_parameters.attachment_type = xml_parameters.template_specifications[xml_parameters.object_id]['attachment_type']
    xml_parameters.noise_std = xml_parameters.template_specifications[xml_parameters.object_id]['noise_std']
    xml_parameters.object_kernel_type = xml_parameters.template_specifications[xml_parameters.object_id]['kernel_type']
    xml_parameters.object_kernel_width = xml_parameters.template_specifications[xml_parameters.object_id][
        'kernel_width']
    xml_parameters.kernel_type = xml_parameters.template_specifications[xml_parameters.object_id]['kernel_type']
    xml_parameters.model_type = 'Shooting'

    groups_list = list(filter(None, re.split('\(|\)', groups)))
    groups_list_end = []
    for l in groups_list:
        list_tmp = []
        groups_n = re.split(',', l)
        for n in groups_n:
            if n.find(':'):
                val = re.split(':', n)
                if len(val) == 2:
                    list_tmp.extend(list(range(int(val[0]), int(val[1]) + 1)))
                elif len(val) == 1:
                    list_tmp.append(int(val[0]))
                else:
                    raise Exception('Wrong format for the groups: %s' % groups)
            else:
                list_tmp.extend(int(n))
        groups_list_end.append(list_tmp)

    momenta = read_3D_array(root_directory / 'output' / 'DeterministicAtlas__EstimatedParameters__Momenta.txt')

    groups_name_list = list(filter(None, re.split(',', group_names)))

    cpt = 0
    output_directory = 'mean_shapes'
    Path.mkdir(root_directory / output_directory, exist_ok=True)
    for g in groups_list_end:
        g = np.array(g)
        moment_g = np.mean(momenta[g-1, :, :], axis=0)
        Path.mkdir(root_directory / output_directory / groups_name_list[cpt], exist_ok=True)
        copyfile(root_directory / 'output' / 'DeterministicAtlas__EstimatedParameters__ControlPoints.txt',
                 root_directory / output_directory / groups_name_list[
                     cpt] / 'DeterministicAtlas__EstimatedParameters__ControlPoints.txt')
        copyfile(root_directory / 'output' / (
                    'DeterministicAtlas__EstimatedParameters__Template_%s.vtk' % xml_parameters.object_id),
                 root_directory / output_directory / groups_name_list[cpt] / (
                     'DeterministicAtlas__EstimatedParameters__Template_%s.vtk' % xml_parameters.object_id))
        copyfile(root_directory / 'optimization_parameters.xml',
                 root_directory / output_directory / groups_name_list[cpt] / 'optimization_parameters.xml')
        xml_parameters.initial_momenta = 'momenta_%s.txt' % groups_name_list[cpt]
        write_3D_array(moment_g, root_directory / output_directory / groups_name_list[cpt],
                       xml_parameters.initial_momenta)
        write_model_xml_shooting(root_directory / output_directory / groups_name_list[cpt], xml_parameters)
        write_launch_simulation_shooting_sh(root_directory / output_directory / groups_name_list[cpt])
        with open(root_directory / output_directory / groups_name_list[cpt] / 'group_list.txt', "w") as f:
            for number in groups_list_end[cpt]:
                f.write("%d " % number)
            f.write("\n")
            for i in groups_list_end[cpt]:
                f.write("%s\n" % path.basename(xml_parameters.dataset_filenames[i-1][0][xml_parameters.object_id]))
        cpt += 1


def main():
    # window for choosing a directory
    root = Tk()
    root.withdraw()  # use to hide tkinter window
    root_directory = Path(filedialog.askdirectory(initialdir="~/", title='Please select a directory (e.g. outupt foldeer)'))

    # window for choosing the number of subjects
    root = Tk()
    root.withdraw()  # use to hide tkinter window
    groups = simpledialog.askstring("Input", "Define the groups (e.g. '(1:4,5)(7:12)(6,13:20,21:32)' (without the quotes); for 1 group, leave empty)", parent=root)

    root = Tk()
    root.withdraw()  # use to hide tkinter window
    group_names = simpledialog.askstring("Input", "Define the group names (e.g. 'Hs,Gg,Pt' (without the quotes, don't use space))", parent=root)

    setup_ShootAndFlow_mean(root_directory, groups, group_names)


if __name__ == "__main__":
    main()


