#!/usr/bin/env python3

"""
Created on Wed Feb 13 12:15:42 2019

@author:  Jean Dumoncel

This function generate pairwise matching to be launched with deformetrica when adding surfaces. From a dataset of
surfaces, every pair with a second dataset of surfaces will be generated.

All the files should be put in a directory ROOT which should looks contains:
ROOT
   | -surfaces
             |surface1.vtk
             |surfaces2.vtk
             |...
   |surfaces_added
                 |surfaces_added1.vtk
                 |surfaces_added2.vtk
                 |...
   |data_set.xml
   |model.xml
   |optimization_parameters.xml
   |Atlas_ControlPointsFixed.txt
   
   where surfacesi.vtk are the surface file in VTK format. 
         surfaces_addedi.vtk are the VTK to be added. For N surfaces_added and P surfaces, this program will produce
         (N*P+N-1) matching folders.
         data_set.xml: see deformetrica manual
         model.xml: see deformetrica manual
         optimization_parameters.xml: see deformetrica manual
         Atlas_ControlPointsFixed.txt: a predefined control points dataset
         
The program asks for the path to the ROOT directory and for the name of the output directory.
         
"""


from pathlib import Path
from shutil import copyfile
from math import ceil
from tkinter import filedialog
from tkinter import *


def server_matching_combination():
    # window for choosing a directory
    root = Tk()
    root.withdraw()  # use to hide tkinter window
    root_directory = Path(filedialog.askdirectory(initialdir="~/",  title='Please select the input directory'))
    # window for choosing a directory
    root = Tk()
    root.withdraw()  # use to hide tkinter window
    output_directory = filedialog.askdirectory(initialdir="~/",  title='Please select the output directory')

    # Parameters
    vtk_directory = root_directory / 'surfaces'  # directory containing surfaces
    vtk_directory_added = root_directory / 'surfaces_added'  # directory containing surfaces
    paramoptimization_parameters = root_directory / 'optimization_parameters.xml'  # optimization parameters filename
    data_set = root_directory / 'data_set.xml'  # data set filename
    model = root_directory / 'model.xml'  # model filename
    control_points = root_directory / 'Atlas_ControlPointsFixed.txt'  # control points filename
    nb_process = 7  # number of process to launch
    clean_files = True  # option for removing some output files
    ######################

    # input contains all combinations
    output_directory_input = 'input'  # directory name containing all the combinations
    Path.mkdir(root_directory / output_directory, exist_ok=True)
    Path.mkdir(root_directory / output_directory / output_directory_input, exist_ok=True)

    # list vtk files
    list_vtk = list(vtk_directory.glob('**/*.vtk'))
    list_vtk_added = list(vtk_directory_added.glob('**/*.vtk'))

    # number of jobs
    nb_surfaces = len(list_vtk)
    nb_surfaces_added = len(list_vtk_added)
    nb_directory = (nb_surfaces * nb_surfaces_added) * 2 + (nb_surfaces_added ** 2 - nb_surfaces_added)

    # copy configuration files
    copyfile(paramoptimization_parameters, root_directory / output_directory / paramoptimization_parameters.name)
    copyfile(control_points, root_directory / output_directory / control_points.name)

    # number of combinations per process
    lim = ceil(nb_directory / nb_process)
    # open main batch
    cpt_fid = 0
    fid_main = open(root_directory/output_directory/('batch_MAIN_%03d.sh' % (cpt_fid+1)), 'w')
    # open sub batch
    fid_submain = open(root_directory/output_directory/'batch_MAIN.sh', 'w')

    # create combinations (Si vs Sj)
    cpt_file = 0
    cpt = 1
    for k in range(len(list_vtk_added)):
        for m in range(len(list_vtk)):
            # combination Sk vs Sm
            directory_name = '%s_to_%s' % (list_vtk_added[k].stem, list_vtk[m].stem)
            Path.mkdir(root_directory / output_directory / output_directory_input / directory_name, exist_ok=True)
            copyfile(list_vtk_added[k], root_directory / output_directory / output_directory_input / directory_name /
                     'input1.vtk')
            copyfile(list_vtk[m], root_directory / output_directory / output_directory_input / directory_name /
                     'input2.vtk')
            copyfile(data_set, root_directory / output_directory / output_directory_input / directory_name /
                     data_set.name)
            copyfile(model, root_directory / output_directory / output_directory_input / directory_name / model.name)
            # open launch
            fid = open(root_directory/output_directory/output_directory_input/directory_name/'launch.sh', 'w')
            fid.write('source activate deformetrica\n\n')
            fid.write('# Registration :\n')
            fid.write('deformetrica estimate model.xml data_set.xml -p ../../optimization_parameters.xml '
                      '--output=output > logout.txt\n')
            fid.close()
            if cpt_file % lim == 0:
                fid_main.close()
                cpt_fid = cpt_fid + 1
                fid_main = open(root_directory/output_directory/('batch_MAIN_%03d.sh' % cpt_fid), 'w')
                fid_submain.write('./batch_MAIN_%03d.sh &\n' % cpt_fid)
            if cpt_file % lim == 0:
                fid_main.write('cd %s\n' % ('input/' + directory_name))
            else:
                fid_main.write('cd %s\n' % ('../' + directory_name))
            if cpt_fid == 1:
                fid_main.write('echo %d / %d\n' % (cpt_file+1, lim))
            fid_main.write('chmod +x launch.sh\n')
            fid_main.write('./launch.sh \n')
            if clean_files:
                fid_main.write('if [ -d "./output" ];then\nfind ./ \\( -name "DeterministicAtlas__*.vtk" -a ! '
                               '-name "*19*" \\) | xargs rm\nfi\n')
            cpt = cpt + 1
            cpt_file = cpt_file + 1

            # combination Sm vs Sk
            directory_name = '%s_to_%s' % (list_vtk[m].stem, list_vtk_added[k].stem)
            Path.mkdir(root_directory / output_directory / output_directory_input / directory_name, exist_ok=True)
            copyfile(list_vtk[m], root_directory / output_directory / output_directory_input / directory_name /
                     'input1.vtk')
            copyfile(list_vtk_added[k], root_directory / output_directory / output_directory_input / directory_name /
                     'input2.vtk')
            copyfile(data_set, root_directory / output_directory / output_directory_input / directory_name
                     / data_set.name)
            copyfile(model, root_directory / output_directory / output_directory_input / directory_name / model.name)
            # open launch
            fid = open(root_directory/output_directory/output_directory_input/directory_name/'launch.sh', 'w')
            fid.write('source activate deformetrica\n\n')
            fid.write('# Registration :\n')
            fid.write('deformetrica estimate model.xml data_set.xml -p ../../optimization_parameters.xml '
                      '--output=output > logout.txt\n')
            fid.close()
            if cpt_file % lim == 0:
                fid_main.close()
                cpt_fid = cpt_fid+1
                fid_main = open(root_directory/output_directory/('batch_MAIN_%03d.sh' % cpt_fid), 'w')
                fid_submain.write('./batch_MAIN_%03d.sh &\n' % cpt_fid)
            if cpt_file % lim == 0:
                fid_main.write('cd %s\n' % ('input/' + directory_name))
            else:
                fid_main.write('cd %s\n' % ('../' + directory_name))
            if cpt_fid == 1:
                fid_main.write('echo %d / %d\n' % (cpt_file+1, lim))
            fid_main.write('chmod +x launch.sh\n')
            fid_main.write('./launch.sh \n')
            if clean_files:
                fid_main.write('if [ -d "./output" ];then\nfind ./ \\( -name "DeterministicAtlas__*.vtk" -a ! '
                               '-name "*19*" \\) | xargs rm\nfi\n')
            cpt = cpt+1
            cpt_file = cpt_file + 1
    
    for k in range(len(list_vtk_added)):
        for m in range(len(list_vtk_added)):
            if k != m:
                # combination Sk vs Sm
                directory_name = '%s_to_%s' % (list_vtk_added[k].stem, list_vtk_added[m].stem)
                Path.mkdir(root_directory / output_directory / output_directory_input / directory_name, exist_ok=True)
                copyfile(list_vtk_added[k], root_directory / output_directory / output_directory_input / directory_name
                         / 'input1.vtk')
                copyfile(list_vtk_added[m], root_directory / output_directory / output_directory_input / directory_name
                         / 'input2.vtk')
                copyfile(data_set, root_directory / output_directory / output_directory_input / directory_name /
                         data_set.name)
                copyfile(model, root_directory / output_directory / output_directory_input / directory_name /
                         model.name)
                # open launch
                fid = open(root_directory/output_directory/output_directory_input/directory_name/'launch.sh', 'w')
                fid.write('source activate deformetrica\n\n')
                fid.write('# Registration :\n')
                fid.write('deformetrica estimate model.xml data_set.xml -p ../../optimization_parameters.xml '
                          '--output=output > logout.txt\n')
                fid.close() 
                if cpt_file % lim == 0:
                    fid_main.close() 
                    cpt_fid = cpt_fid+1
                    fid_main = open(root_directory/output_directory/('batch_MAIN_%03d.sh' % cpt_fid), 'w')
                    fid_submain.write('./batch_MAIN_%03d.sh &\n' % cpt_fid)
                    fid_main.write('cd %s\n' % ('input/' + directory_name)) 
                else:
                    fid_main.write('cd %s\n' % ('../' + directory_name))
                if cpt_fid == 1:
                    fid_main.write('echo %d / %d\n' % (cpt_file+1, lim))
                fid_main.write('chmod +x launch.sh\n')
                fid_main.write('./launch.sh \n')
                if clean_files:
                    fid_main.write('if [ -d "./output" ];then\nfind ./ \\( -name "DeterministicAtlas__*.vtk" -a ! '
                                   '-name "*19*" \\) | xargs rm\nfi\n')
                cpt = cpt + 1
                cpt_file = cpt_file + 1
    
    fid_main.close() 
    fid_submain.close() 


def main():
    """
    main
    """
    server_matching_combination()


if __name__ == "__main__":
    main() 
