#!/usr/bin/env python3

"""
Created on Tue Mar  5 13:55:58 2019

@author: Jean Dumoncel

This function generate pairwise matching to be launched with deformetrica on a supercomputer using SLURM and chdb when
adding surfaces. From a dataset of surfaces, every pair with a second dataset of surfaces will be generated.

All the files should be put in a directory ROOT which should looks contains:
ROOT
   | -surfaces
             |surface1.vtk
             |surfaces2.vtk
             |...
   |surfaces_added
                 |surfaces_added1.vtk
                 |surfaces_added2.vtk
                 |...
   |data_set.xml
   |model.xml
   |optimization_parameters.xml
   |Atlas_ControlPointsFixed.txt
   
   where surfacesi.vtk are the surface file in VTK format. 
         surfaces_addedi.vtk are the VTK to be added. For N surfaces_added and P surfaces, this program will produce
         (N*P+N-1) matching folders.
         data_set.xml: see deformetrica manual
         model.xml: see deformetrica manual
         optimization_parameters.xml: see deformetrica manual
         Atlas_ControlPointsFixed.txt: a predefined control points dataset
         
The program asks for the path to the ROOT directory and for the name of the output directory.
         
"""

from pathlib import Path
from shutil import copyfile
from tkinter import filedialog
from tkinter import *
from tkinter import simpledialog
from server_matching_combination_olympe import write_launch_simulation_chdb


def server_matching_combination_olympe():
    # window for choosing a directory
    root = Tk()
    root.withdraw()  # use to hide tkinter window
    root_directory = Path(filedialog.askdirectory(initialdir="~/",  title='Please select the input directory'))
    # window for choosing a directory
    root = Tk()
    root.withdraw()  # use to hide tkinter window
    output_directory = filedialog.askdirectory(initialdir="~/",  title='Please select the output directory')
    # window for email address
    root = Tk()
    root.withdraw()  # use to hide tkinter window
    email_address = simpledialog.askstring("Input", "What is your email address?",
                                           initialvalue="jean.dumoncel@univ-tlse3.fr", parent=root)
    # window for number of gpus
    root = Tk()
    root.withdraw()  # use to hide tkinter window
    number_of_gpus = simpledialog.askstring("Input", "What is the number of gpus?", parent=root)
    # window for number of tasks per node
    root = Tk()
    root.withdraw()  # use to hide tkinter window
    number_of_tasks_per_node = simpledialog.askstring("Input", "What is the number of tasks per node?", parent=root)
    
    # Parameters
    vtk_directory = root_directory / 'surfaces'  # directory containing surfaces
    vtk_directory_added = root_directory / 'surfaces_added'  # directory containing surfaces
    paramoptimization_parameters = root_directory / 'optimization_parameters.xml'  # optimization parameters filename
    data_set = root_directory / 'data_set.xml'  # data set filename
    model = root_directory / 'model.xmli'  # model filename
    control_points = root_directory / 'Atlas_ControlPointsFixed.txt'  # control points filename
    # nb_process = 7  # number of process to launch
    ######################

    # input contains all combinations
    output_directory_input = 'input'  # directory name containing all the combinations
    Path.mkdir(root_directory / output_directory, exist_ok=True)
    Path.mkdir(root_directory / output_directory / output_directory_input, exist_ok=True)

    # list vtk files
    list_vtk = list(vtk_directory.glob('**/*.vtk'))
    list_vtk_added = list(vtk_directory_added.glob('**/*.vtk'))

    # copy files in output directory
    copyfile(paramoptimization_parameters, root_directory / output_directory / paramoptimization_parameters.name)
    copyfile(control_points, root_directory / output_directory / control_points.name)

    write_launch_simulation_chdb(root_directory, output_directory, email_address, number_of_gpus,
                                 number_of_tasks_per_node)

    # create combinations (Si vs Sj)
    for k in range(len(list_vtk_added)):
        for m in range(len(list_vtk)):
                # combination Sk vs Sm
                directory_name = '%s_to_%s' % (list_vtk_added[k].stem, list_vtk[m].stem)
                Path.mkdir(root_directory / output_directory / output_directory_input / directory_name, exist_ok=True)
                copyfile(list_vtk_added[k], root_directory / output_directory / output_directory_input / directory_name
                         / 'input1.vtk')
                copyfile(list_vtk[m], root_directory / output_directory / output_directory_input / directory_name /
                         'input2.vtk')
                copyfile(data_set, root_directory / output_directory / output_directory_input / directory_name /
                         data_set.name)
                copyfile(model, root_directory / output_directory / output_directory_input / directory_name /
                         model.name)

                # combination Sm vs Sk
                directory_name = '%s_to_%s' % (list_vtk[m].stem, list_vtk_added[k].stem)
                Path.mkdir(root_directory / output_directory / output_directory_input / directory_name, exist_ok=True)
                copyfile(list_vtk[m], root_directory / output_directory / output_directory_input / directory_name /
                         'input1.vtk')
                copyfile(list_vtk_added[k], root_directory / output_directory / output_directory_input / directory_name
                         / 'input2.vtk')
                copyfile(data_set, root_directory / output_directory / output_directory_input / directory_name /
                         data_set.name)
                copyfile(model, root_directory / output_directory / output_directory_input / directory_name /
                         model.name)
    
    for k in range(len(list_vtk_added)):
        for m in range(len(list_vtk_added)):
            if k != m:
                # combination Sk vs Sm
                directory_name = '%s_to_%s' % (list_vtk_added[k].stem, list_vtk_added[m].stem)
                Path.mkdir(root_directory / output_directory / output_directory_input / directory_name, exist_ok=True)
                copyfile(list_vtk_added[k], root_directory / output_directory / output_directory_input / directory_name
                         / 'input1.vtk')
                copyfile(list_vtk_added[m], root_directory / output_directory / output_directory_input / directory_name
                         / 'input2.vtk')
                copyfile(data_set, root_directory / output_directory / output_directory_input / directory_name /
                         data_set.name)
                copyfile(model, root_directory / output_directory / output_directory_input / directory_name /
                         model.name)


def main():
    """
    main
    """
    server_matching_combination_olympe()


if __name__ == "__main__":
    main() 
