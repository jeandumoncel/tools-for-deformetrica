#!/usr/bin/env python3

"""
Created on Mon Apr 9 09:26:08 2019

@author: Jean Dumoncel

This function generate pairwise matching to be launched with deformetrica on a supercomputer using SLURM and chdb. From
a dataset of surfaces, every pair will be generated.

All the files should be put in a directory ROOT which should looks contains:
ROOT
   | -surfaces
             |surface1.vtk
             |surfaces2.vtk
             |...
   |data_set.xml
   |model.xml
   |optimization_parameters.xml
   |Atlas_ControlPointsFixed.txt

   where surfacesi.vtk are the surface file in VTK format. For N files, this program will produce N^2-N matching
   folders.
         data_set.xml: see deformetrica manual
         model.xml: see deformetrica manual
         optimization_parameters.xml: see deformetrica manual
         Atlas_ControlPointsFixed.txt: a predefined control points dataset

The program asks for the path to the ROOT directory and for the name of the output directory.

"""

from pathlib import Path
from shutil import copyfile
from tkinter import filedialog
from tkinter import *
from tkinter import simpledialog

def write_launch_simulation_chdb(root_directory, output_directory, email_address, number_of_gpus,
                                 number_of_tasks_per_node):
    # generate a SLURM file for Olympe
    fid_slurm = open(root_directory / output_directory / 'chdb.bash', 'w')
    fid_slurm.write('#!/bin/bash\n')
    fid_slurm.write('#SBATCH -J project\n')
    fid_slurm.write('#SBATCH -N 1\n')
    fid_slurm.write('#SBATCH -n %s\n' % number_of_tasks_per_node)
    fid_slurm.write('#SBATCH --ntasks-per-node=%s\n' % number_of_tasks_per_node)
    fid_slurm.write('#SBATCH --ntasks-per-core=1\n')
    fid_slurm.write('#SBATCH --gres=gpu:%s\n' % number_of_gpus)
    fid_slurm.write('#SBATCH --time=40:00:00\n')
    fid_slurm.write('#SBATCH --mail-user=%s\n' % email_address)
    fid_slurm.write('#SBATCH --mail-type=ALL\n\n')
    fid_slurm.write('module purge\n')
    fid_slurm.write('module load python/3.6.3\n')
    fid_slurm.write('module load cuda/9.0.176.2\n')
    fid_slurm.write('source activate deformetrica\n')
    fid_slurm.write('module load chdb/1.0\n\n')
    fid_slurm.write('export I_MPI_PMI_LIBRARY=/usr/lib64/libpmi.so\n\n')
    fid_slurm.write('DEFORMETRICA=deformetrica\n\n')
    fid_slurm.write('LAUNCHDIR=$(pwd)\n')
    fid_slurm.write('INPUTDIR=$(pwd)/input\n')
    fid_slurm.write('OUTPUTDIR=$(pwd)/VTKOUTPUT\n\n')
    fid_slurm.write('OPTIMIZATIONPARAMETERS=$LAUNCHDIR/optimization_parameters.xml\n\n')
    fid_slurm.write('srun chdb \\\n')
    fid_slurm.write('--verbose \\\n')
    fid_slurm.write('--report report-${SLURM_JOBID} \\\n')
    fid_slurm.write('--in-dir $INPUTDIR \\\n')
    fid_slurm.write('--in-type xmli \\\n')
    fid_slurm.write('--out-dir ${OUTPUTDIR}-${SLURM_JOBID} \\\n')
    fid_slurm.write('--out-files %out-dir%/%path%.logout,%out-dir%/Atlas_ControlPoints.txt,%out-dir%/'
                    'Atlas_InitialMomentas.txt \\\n')
    fid_slurm.write('--command-line  "(cd %in-dir%/%dirname% ; export CUDA_VISIBLE_DEVICES=\$(expr \$CHDB_RANK % 4) ; '
                    '$DEFORMETRICA estimate model.xmli data_set.xml -p $OPTIMIZATIONPARAMETERS --output=output > '
                    'logout.txt)"\n')
    fid_slurm.close()


def server_matching_combination():
    # window for choosing a directory
    root = Tk()
    root.withdraw()  # use to hide tkinter window
    root_directory = Path(filedialog.askdirectory(initialdir="~/", title='Please select a directory containing files'))
    # window for choosing a directory
    root = Tk()
    root.withdraw()  # use to hide tkinter window
    output_directory = simpledialog.askstring("Input", "What is the output directory name?", parent=root)
    # window for email address
    root = Tk()
    root.withdraw()  # use to hide tkinter window
    email_address = simpledialog.askstring("Input", "What is your email address?",
                                           initialvalue="jean.dumoncel@univ-tlse3.fr", parent=root)
    # window for number of gpus
    root = Tk()
    root.withdraw()  # use to hide tkinter window
    number_of_gpus = simpledialog.askstring("Input", "What is the number of gpus?", parent=root)
    # window for number of tasks per node
    root = Tk()
    root.withdraw()  # use to hide tkinter window
    number_of_tasks_per_node = simpledialog.askstring("Input", "What is the number of tasks per node?", parent=root)

    # Parameters
    vtk_directory = root_directory / 'surfaces'  # directory containing surfaces
    paramoptimization_parameters = root_directory / 'optimization_parameters.xml'  # optimization parameters filename
    data_set = root_directory / 'data_set.xml'  # data set filename
    model = root_directory / 'model.xmli'  # model filename
    control_points = root_directory / 'Atlas_ControlPointsFixed.txt'  # control points filename
    # nb_process = 7  # number of process to launch
    # clean_files = True  # option for removing some output files
    ######################

    # input contains all combinations
    output_directory_input = 'input'  # directory name containing all the combinations
    Path.mkdir(root_directory / output_directory, exist_ok=True)
    Path.mkdir(root_directory / output_directory / output_directory_input, exist_ok=True)

    # list vtk files
    list_vtk = list(vtk_directory.glob('**/*.vtk'))

    # number of jobs
    # nb_surfaces = len(list_vtk)

    # copy configuration files
    copyfile(paramoptimization_parameters, root_directory / output_directory / paramoptimization_parameters.name)
    copyfile(control_points, root_directory / output_directory / control_points.name)

    write_launch_simulation_chdb(root_directory, output_directory, email_address, number_of_gpus,
                                 number_of_tasks_per_node)

    for k in range(len(list_vtk)):
        for m in range(len(list_vtk)):
            if k != m:
                # combination Sk vs Sm
                directory_name = '%s_to_%s' % (list_vtk[k].stem, list_vtk[m].stem)
                Path.mkdir(root_directory / output_directory / output_directory_input / directory_name, exist_ok=True)
                copyfile(list_vtk[k],
                         root_directory / output_directory / output_directory_input / directory_name / 'input1.vtk')
                copyfile(list_vtk[m],
                         root_directory / output_directory / output_directory_input / directory_name / 'input2.vtk')
                copyfile(data_set,
                         root_directory / output_directory / output_directory_input / directory_name / data_set.name)
                copyfile(model,
                         root_directory / output_directory / output_directory_input / directory_name / model.name)


def main():
    """
    main
    """
    server_matching_combination()


if __name__ == "__main__":
    main()
